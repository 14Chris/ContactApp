package hello;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.MessageCodesResolver;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import hello.model.Contact;
import hello.model.ContactRepository;


@Controller
public class WebController implements WebMvcConfigurer {

	@Autowired
	ContactRepository repository;
	
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
    }
    
    @GetMapping("/")
    public String showFormList() {
        return "ContactList";
    }
    
    @GetMapping("/ContactList")
    public String showForm() {
    	return "redirect:/ContactList";
    }
    
    @ModelAttribute("ListeContact")
    public List<Contact> getListeContact(){
    	return repository.findAll();
    }

    @GetMapping("AddContact")
    public String showForm(Contact contact) {
    	    	
        return "ContactForm";
    }
    

    @GetMapping(value = "/ModifContact/{id}")
    public String ModifContact(Contact contact, Model model, @PathVariable("id") long id) {
    	
    	Contact c = new Contact();
    	
    	if(id>0) {
    		c = repository.findOne(id);
    		contact = c;
    	}
    	
    	model.addAttribute("contact", c);
       	
    	return "ContactModifForm";
    }
    
    
    @PostMapping(value = "/ModifContact")
    public String ModifContact(@ModelAttribute Contact contact) {
    	
    	repository.save(contact); 
       	
    	return "redirect:/ContactList";
    }
    
    @RequestMapping("/deleteContact/{id}")
    public String deleteContact(@PathVariable("id") long id) {
    	

    	if(id>0) {
    		repository.delete(id);
    	}		

    	return "redirect:/ContactList";
    }

    @PostMapping("/AddContact")
    public String checkContactInfo(@Valid Contact contact, BindingResult bindingResult) {
    	
        if (bindingResult.hasErrors()) {
            return "ContactForm";
        }
        
        repository.save(contact);

        return "redirect:/ListContact";
    }

	@Override
	public void configurePathMatch(PathMatchConfigurer configurer) {
		
	}

	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		
	}

	@Override
	public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
		
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		
	}

	@Override
	public void addFormatters(FormatterRegistry registry) {
		
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		
	}

	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		
	}

	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		
	}

	@Override
	public void addReturnValueHandlers(List<HandlerMethodReturnValueHandler> returnValueHandlers) {
		
	}

	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		
	}

	@Override
	public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
		
	}

	@Override
	public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
		
	}

	@Override
	public void extendHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
		
	}

	@Override
	public Validator getValidator() {
		return null;
	}

	@Override
	public MessageCodesResolver getMessageCodesResolver() {
		return null;
	}
}