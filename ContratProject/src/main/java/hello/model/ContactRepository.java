package hello.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {

    List<Contact> findByPrenom(String prenom);
    
    List<Contact> findByNom(String nom);
    
    List<Contact> findAll();
    
}
