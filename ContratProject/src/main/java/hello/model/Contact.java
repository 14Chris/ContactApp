package hello.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Contact {
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String prenom;
    private String nom;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateNaissance;
    private String telephone;
    private String email;
    
    public Contact() {}

    public Contact(String prenom, String nom, Date dateNaiss, String tel, String email) {
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaiss;
        this.email = email;
        this.telephone = tel;
    }

    @Override
    public String toString() {
        return String.format(
                "Contact[id=%d, prenom='%s', nom='%s']",
                id, prenom, nom);
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
    
    
    
}
