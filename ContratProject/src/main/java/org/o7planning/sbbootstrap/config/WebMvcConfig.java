package org.o7planning.sbbootstrap.config;
 
import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.validation.MessageCodesResolver;
import org.springframework.validation.Validator;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
 
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
 
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //
        // Access Bootstrap static resource:
        //
        // http://somedomain/SomeContextPath/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css
        //
        registry.addResourceHandler("/webjars/**") //
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

	@Override
	public void configurePathMatch(PathMatchConfigurer configurer) {
		// TODO Stub de la m�thode g�n�r� automatiquement
		
	}

	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		// TODO Stub de la m�thode g�n�r� automatiquement
		
	}

	@Override
	public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
		// TODO Stub de la m�thode g�n�r� automatiquement
		
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		// TODO Stub de la m�thode g�n�r� automatiquement
		
	}

	@Override
	public void addFormatters(FormatterRegistry registry) {
		// TODO Stub de la m�thode g�n�r� automatiquement
		
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// TODO Stub de la m�thode g�n�r� automatiquement
		
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		// TODO Stub de la m�thode g�n�r� automatiquement
		
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		// TODO Stub de la m�thode g�n�r� automatiquement
		
	}

	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		// TODO Stub de la m�thode g�n�r� automatiquement
		
	}

	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		// TODO Stub de la m�thode g�n�r� automatiquement
		
	}

	@Override
	public void addReturnValueHandlers(List<HandlerMethodReturnValueHandler> returnValueHandlers) {
		// TODO Stub de la m�thode g�n�r� automatiquement
		
	}

	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		// TODO Stub de la m�thode g�n�r� automatiquement
		
	}

	@Override
	public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
		// TODO Stub de la m�thode g�n�r� automatiquement
		
	}

	@Override
	public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
		// TODO Stub de la m�thode g�n�r� automatiquement
		
	}

	@Override
	public void extendHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
		// TODO Stub de la m�thode g�n�r� automatiquement
		
	}

	@Override
	public Validator getValidator() {
		// TODO Stub de la m�thode g�n�r� automatiquement
		return null;
	}

	@Override
	public MessageCodesResolver getMessageCodesResolver() {
		// TODO Stub de la m�thode g�n�r� automatiquement
		return null;
	}
 
}